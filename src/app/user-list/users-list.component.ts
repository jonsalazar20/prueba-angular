import { Component, OnInit, ViewChild, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { RouterStateSnapshot } from '@angular/router';
import { DatatableComponent, ColumnMode } from "@swimlane/ngx-datatable";
import { Observable } from 'rxjs';
import { corporative } from '../user-list/users-list.interface';
import { CorporativeService } from './../generales/_services/corporative.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: "app-users-list",
  templateUrl: "./users-list.component.html",
  styleUrls: [
    "./users-list.component.scss",
    "/assets/sass/libs/datatables.scss"
  ],
  encapsulation: ViewEncapsulation.None,
})
export class UsersListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  // row data
  corporativesData: corporative[] = [];
  public rows;
  public ColumnMode = ColumnMode;

  // private
  private tempData = [];
  empty: number
  corporativos
  prueba
  constructor(private corporativeService: CorporativeService,
    private route: ActivatedRoute) {
    // this.getParamsId()
    this.tempData = this.rows;
  }
  ngOnInit(): void {
    this.getData();
    this.getParams(16);
  }

  // Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * filterUpdate
   *
   * @param event
   */


  getData() {
    this.corporativeService.getAllCorporatives()
      .subscribe(response => {
        console.log(response);
        this.rows = response.data
        console.log(this.rows);
      })

  }

  // getParamsId() {
  //   this.route.params
  //     .subscribe((params: Params) => {
  //       console.log(params['id'])
  //       // const id = params.id
  //       // this.corporativos = this.corporativeService.getCorporatives(id)
  //       // console.log(this.corporativos.id);  
  //     })
  // }

  getParams(id = 16){
    this.corporativeService.getCorporatives(id)
    .subscribe(response=> console.log(response)
    )
  }
}
