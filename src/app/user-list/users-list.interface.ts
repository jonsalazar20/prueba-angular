export interface corporative {
    id: number,
    S_NombreCorto: string,
    S_NombreCompleto: string,
    S_LogoURL: string,
    D_FechaIncorporacion: string
    S_SystemUrl: string,
    S_Apellidos: string,
    S_Nombre: string
}