import { Routes, RouterModule } from '@angular/router';

//Route for content layout with sidebar, navbar and footer.

export const Full_ROUTES: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../../dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'corporativos',
    loadChildren: () => import('../../user-list/users-list.module').then(m => m.UsersListModule)
  },
  {
    path: 'corporativos/:id',
    loadChildren: () => import ('../../users-edit/users.edit.module')
    .then(m => m.UsersEditModule)

  }

];
