import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { UsersListRoutingModule } from "./users-edit.routing.module";

import { UsersEditComponent } from "./users-edit.component";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from "@ng-select/ng-select";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from '@angular/forms';





@NgModule({
  imports: [
    CommonModule,
    UsersListRoutingModule,
    NgxDatatableModule,
    NgSelectModule,
    NgbModule,
    FormsModule
    
  ],
  exports: [],
  declarations: [
  
    UsersEditComponent
],
  providers: [],
})
export class UsersEditModule { }
