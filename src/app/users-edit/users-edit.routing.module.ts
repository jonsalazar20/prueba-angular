import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersEditComponent } from './users-edit.component';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";




const routes: Routes = [
  {
    path: 'corporativos/:id',
    component: UsersEditComponent,
    data: {
      title: 'corporativos/:id'
    }

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule,
    NgbModule],
})
export class UsersListRoutingModule { }
