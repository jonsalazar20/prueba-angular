import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CorporativeService  }  from '../generales/_services/corporative.service';
import { ActivatedRoute, Params} from '@angular/router';
@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.scss', '/assets/sass/pages/page-users.scss', '/assets/sass/libs/select.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UsersEditComponent implements OnInit {
  heroe = {}
  corporative 
  constructor(private corporativeService : CorporativeService,
              private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.getParamsId()
  }

  // getIdCorporatives(id){
  //   this.corporativeService.getCorporatives(id)
  //   .subscribe(response => id === response.id)
  // }

  getParamsId(){
    this.route.params
    .subscribe((params:Params)=>{
      this.corporative = this.corporativeService.getCorporatives( params['id'] );
    })
  }
}
